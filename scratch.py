import requests
from datetime import datetime
import isoduration

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


API_KEY = "AIzaSyCvMY7xY5r9GDJBsgSgG8Ok9E9hLU1Pqss"

videoID = 'elgTG9oDiJY'


base_url = 'https://www.googleapis.com/youtube/v3/commentThreads'
params = {
    'part': 'snippet',
    'videoId': videoID,
    'key': API_KEY,
    'maxResults': 100,  # You can adjust this value to get more comments per request
}

commentDates = []
commentAuthors = []

while True:
    response = requests.get(base_url, params=params)
    data = response.json()
    
    for item in data['items']:
        cData = item['snippet']['topLevelComment']['snippet']
        date = datetime.fromisoformat(cData['publishedAt'].replace('Z', '+00:00'))
        commentDates.append(date)
        
        author = cData['authorChannelId']['value']
        commentAuthors.append(author)
        
    
    if 'nextPageToken' in data:
        params['pageToken'] = data['nextPageToken']
    else:
        break
    
    
comments = pd.Series(index=commentAuthors, data=commentDates)
 
    
base_url = 'https://www.googleapis.com/youtube/v3/videos'
params = {
    'part': 'snippet,contentDetails,statistics',
    'id': videoID,
    'key': API_KEY}

response = requests.get(base_url, params=params)
data = response.json()

publishedAt = data['items'][0]['snippet']['publishedAt']
publishedAt = datetime.fromisoformat(publishedAt.replace('Z', '+00:00'))


duration = data['items'][0]['contentDetails']['duration']
duration = isoduration.parse_duration(duration)
videoDuration = int(
    duration.time.hours*360 + duration.time.minutes*60 + duration.time.seconds)


diff = [(c - publishedAt).seconds/60 for c in commentDates]


dt = 15
bins = range(0, int(round(max(diff))) + 1, int(round(dt)))

# plt.hist(diff, bins=bins)
plt.figure()
sns.histplot(diff, bins=bins)    
# data = get_video_comments("dYDl56mnm-g")